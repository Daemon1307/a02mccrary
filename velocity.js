var App = {
    launch: function () {
        App.getInitVelocity();
        App.getTime();
    },
    getInitVelocity :function(){
        let answer = prompt("Initial Veolocity","0");
        if (answer != null) {
            sessionStorage.setItem("initVelocity",answer);
            $("#initVelocity").html("Initial Velocity "+answer); // $ = jQuery object, uses CSS selectors
            
        }
    },
    getTime :function(){
        let answer = prompt("Time Until Impact","0");
        if (answer != null) {
            sessionStorage.setItem("timeToImpact",answer);
            document.getElementById("timeToImpact").innerHTML("Time: " + answer); // $ = jQuery object, uses CSS selectors
            let final = calculateVelocity( parseFloat(sessionStorage.getItem("initVelocity"), parseFloat(answer)));
            $("#finalVelocity").html(final);
        }
    },
    calculateVelocity : function(initVelocity,timeToImpact){
        if(typeof initVelocity !== 'number' || typeof timeToImpact !== 'number'){
            throw Error('The given argument is not a number');
        }
        if(initVelocity<0)
            {
                initVelocity = 0;
            }
        if(timeToImpact<0)
            {
                initVelocity = 0;
            }
        
        return timeToImpact*initVelocity; // do some checks on the inputs
        
    }
};